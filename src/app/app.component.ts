import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'select-tab';
  homeText = '';
  productText = '';
  galleryText = '';
  aboutText = '';

  getHome(value: any) {
    this.homeText = value;
    this.productText = '';
    this.aboutText = '';
    this.galleryText = '';
  }

  getProduct(value: any) {
    this.productText = value;
    this.homeText = '';
    this.aboutText = '';
    this.galleryText = '';
  }
  getGallery(value: any) {
    this.galleryText = value;
    this.homeText = '';
    this.productText = '';
    this.aboutText = '';
  }
  getAboutUs(value: any) {
    this.aboutText = value;
    this.homeText = '';
    this.productText = '';
    this.galleryText = '';
  }
}
