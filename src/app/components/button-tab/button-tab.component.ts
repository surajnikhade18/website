import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button-tab',
  templateUrl: './button-tab.component.html',
  styleUrls: ['./button-tab.component.scss'],
})
export class ButtonTabComponent implements OnInit {
  @Output() homeContent = new EventEmitter();
  @Output() productContent = new EventEmitter();
  @Output() aboutUsContent = new EventEmitter();
  @Output() galleryContent = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  getHomeContent(value: any) {
    this.homeContent.emit('Called Home');
  }
  getProductContent(value: any) {
    this.productContent.emit('Called Product');
  }

  getAboutUsContent(value: any) {
    this.aboutUsContent.emit('Called AboutUs');
  }

  getGalleryContent(value: any) {
    this.galleryContent.emit('Called Gallery');
  }
}
